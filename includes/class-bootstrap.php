<?php
/**
 * Bootstrap class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\WordpressPhpunitFramework;

/**
 * PHPUnit bootstrap.
 *
 * @since 1.0.0
 */
abstract class Bootstrap {

	/**
	 * Plugin directory path.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $plugin_dir;

	/**
	 * Main PHP File for the plugin.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $plugin_main;

	/**
	 * The directory where the test WordPress environment is
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $tests_dir;

	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		global $phpunit_polyfills_path;

		$wp_test_dir = getenv( 'WP_TESTS_DIR' );
		if ( ! $wp_test_dir ) {
			$wp_test_dir = 'tmp/wordpress-tests-lib';
		}

		$this->plugin_dir = getcwd();

		// Forward custom PHPUnit Polyfills configuration to PHPUnit bootstrap file.
		$phpunit_polyfills_path = getenv( 'WP_TESTS_PHPUNIT_POLYFILLS_PATH' );
		if ( $phpunit_polyfills_path !== false ) {
			define( 'WP_TESTS_PHPUNIT_POLYFILLS_PATH', $phpunit_polyfills_path );
		}

		if ( file_exists( "$wp_test_dir/includes/functions.php" ) ) {
			// Give access to tests_add_filter() function.
			require_once "$wp_test_dir/includes/functions.php";

		} else {
			// phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped
			echo "Could not find $wp_test_dir/includes/functions.php. " .
				"Have you run 'bin/ponder-tests install-wp-suite-db'?" . PHP_EOL;
			exit( 1 );
		}

		// Load the plugin.
		tests_add_filter( 'muplugins_loaded', [ $this, 'load' ] );

		// Install the plugin.
		tests_add_filter( 'setup_theme', array( $this, 'install' ) );

		// Start up the WP testing environment.
		/* @noinspection PhpIncludeInspection */
		require "$wp_test_dir/includes/bootstrap.php";
	}

	/**
	 * Install the plugin being tested and any plugins it is integrated with.
	 *
	 * @since 1.0.0
	 */
	abstract public function install();

	/**
	 * Load the plugin being tested.
	 *
	 * @since 1.0.0
	 */
	public function load() {

		/* @noinspection PhpIncludeInspection */
		require_once "$this->plugin_dir/$this->plugin_main";
	}

	/**
	 * Load a plugin dependency in the WordPress testing environment.
	 *
	 * @since 1.0.0
	 * @param string $dir  Directory name for the plugin that is being loaded.
	 * @param string $file File name of the plugin.
	 */
	public function load_plugin( $dir, $file ) {

		if ( file_exists( WP_PLUGIN_DIR . '/' . $dir ) ) {
			/* @noinspection PhpIncludeInspection */
			require_once WP_PLUGIN_DIR . "/$dir/$file";
		}
	}
}
