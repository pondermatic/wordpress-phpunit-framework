<?php
/**
 * Factory_For_Product_variation class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\WordpressPhpunitFramework;

use WP_UnitTest_Factory_For_Thing;

/**
 * A factory that makes WC_Product_Variation objects for testing.
 *
 * @since 1.0.0
 */
class Factory_For_Product_Variation extends WP_UnitTest_Factory_For_Thing {

	/**
	 * Creates an object.
	 *
	 * @since 1.0.0
	 * @param array $args The arguments.
	 * @return mixed The result. Can be anything.
	 */
	public function create_object( $args ) {
		// TODO: Implement create_object() method.
	}

	/**
	 * Updates an existing object.
	 *
	 * @since 1.0.0
	 * @param int   $object The object ID.
	 * @param array $fields The values to update.
	 * @return mixed The result. Can be anything.
	 */
	public function update_object( $object, $fields ) {
		// TODO: Implement update_object() method.
	}

	/**
	 * Retrieves an object by ID.
	 *
	 * @since 1.0.0
	 * @param int $object_id The object ID.
	 * @return mixed The object. Can be anything.
	 */
	public function get_object_by_id( $object_id ) {
		// TODO: Implement get_object_by_id() method.
	}
}
