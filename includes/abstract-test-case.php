<?php
/**
 * Test_Case class
 *
 * @since 1.0.0
 * @version 1.0.0
 */
namespace Pondermatic\WordpressPhpunitFramework;

use WP_UnitTestCase;

/**
 * Basic abstract test class.
 *
 * All WordPress unit tests should inherit from this class.
 *
 * @since 1.0.0
 */
abstract class Test_Case extends WP_UnitTestCase {

	/**
	 * Fetches the factory object for generating WordPress fixtures.
	 *
	 * @return Factory The fixture factory.
	 */
	protected static function factory() {

		static $factory = null;
		if ( ! $factory ) {
			$factory = new Factory();
		}
		return $factory;
	}
}
