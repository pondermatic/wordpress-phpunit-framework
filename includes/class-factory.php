<?php
/**
 * Factory class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\WordpressPhpunitFramework;

use WP_UnitTest_Factory;

/**
 * A factory that makes objects for testing.
 *
 * @since 1.0.0
 */
class Factory extends WP_UnitTest_Factory {

	/**
	 * Generates WC_Product_Simple fixtures for use in tests.
	 *
	 * @since 1.0.0
	 * @var Factory_For_Product_Simple
	 */
	public $product_simple;

	/**
	 * Generates WC_Product_Variation fixtures for use in tests.
	 *
	 * @since 1.0.0
	 * @var Factory_For_Product_Variation
	 */
	public $product_variation;

	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		parent::__construct();
		$this->product_simple    = new Factory_For_Product_Simple( $this );
		$this->product_variation = new Factory_For_Product_Variation( $this );
	}


}
