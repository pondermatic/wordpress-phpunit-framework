<?php
/**
 * Test_Installer class
 *
 * @since 1.0.0
 * @version 1.0.0
 */

namespace Pondermatic\WordpressPhpunitFramework;

use GetOpt\ArgumentException;
use GetOpt\ArgumentException\Missing;
use GetOpt\Command;
use GetOpt\GetOpt;
use GetOpt\Operand;
use GetOpt\Option;
use mysqli;
use ZipArchive;

/**
 * Installs a WordPress testing system.
 *
 * phpcs:disabled WordPress.DB
 * phpcs:disabled WordPress.Security
 * phpcs:disabled WordPress.WP.AlternativeFunctions
 *
 * @since 1.0.0
 */
class Test_Installer {

	/**
	 * The name of this command.
	 */
	public const NAME = 'ponder-phpunit';

	/**
	 * The version of this command.
	 */
	public const VERSION = '1.0.0';

	/**
	 * A handle to the database server.
	 *
	 * @since 1.0.0
	 * @var mysqli
	 */
	protected $db;

	/**
	 * The GetOpt command line processor.
	 *
	 * @since 1.0.0
	 * @var GetOpt
	 */
	protected $get_opt;

	/**
	 * An array of the latest versions of WordPress and plugins.
	 *
	 * @since 1.0.0
	 * @var string[] Indexed by 'WordPress' or plugin slugs.
	 */
	protected $latest_versions = [];

	/**
	 * The slug of the WordPress plugin.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $slug;

	/**
	 * The directory that contains the test environment.
	 *
	 * @since 1.0.0
	 * @var string
	 */
	protected $test_dir;

	/**
	 * Constructor.
	 *
	 * @since 1.0.0
	 */
	public function __construct() {

		$this->set_up_get_opt();
	}

	/**
	 * Configures the WordPress config file in the test environment.
	 *
	 * @since 1.0.0
	 */
	protected function configure_wordpress() {

		$options = $this->get_opt->getOptions();
		$config  = file_get_contents( "$this->test_dir/wordpress-tests-lib/wp-tests-config.php" );

		/* @noinspection SpellCheckingInspection */
		$replacements = [
			'dirname( __FILE__ ) . \'/src/\'' => "'$this->test_dir/wordpress/'",
			'__DIR__ . \'/src/\''             => "'$this->test_dir/wordpress/'",
			'youremptytestdbnamehere'         => $options['db-name'],
			'yourusernamehere'                => $options['db-user'],
			'yourpasswordhere'                => $options['db-pass'],
			'localhost'                       => $options['db-host'],
		];
		$config       = str_replace( array_keys( $replacements ), $replacements, $config );

		file_put_contents( "$this->test_dir/wordpress-tests-lib/wp-tests-config.php", $config );
	}

	/**
	 * Creates the testing database.
	 *
	 * @since 1.0.0
	 */
	protected function create_database() {

		$db      = $this->get_database_connection();
		$db_name = $this->get_opt->getOption( 'db-name' );
		$db->query( "CREATE DATABASE `$db_name`;" );
	}

	/**
	 * Deletes the testing database.
	 *
	 * @since 1.0.0
	 */
	protected function delete_database() {

		$db      = $this->get_database_connection();
		$db_name = $this->get_opt->getOption( 'db-name' );
		$db->query( "DROP DATABASE `$db_name`;" );
	}

	/**
	 * Removes a directory, the files within a directory, and all subdirectories.
	 *
	 * @since 1.0.0
	 * @param string $dirname The file or directory path to delete.
	 * @return bool `true` if successful, else `false`.
	 */
	protected function delete_directory( $dirname ) {

		if ( ! is_dir( $dirname ) ) {
			return false;
		}
		$dir_handle = opendir( $dirname );
		if ( ! $dir_handle ) {
			return false;
		}

		$file = readdir( $dir_handle );
		while ( $file ) {
			if ( ! in_array( $file, [ '.', '..' ], true ) ) {
				if ( is_dir( "$dirname/$file" ) ) {
					$this->delete_directory( "$dirname/$file" );
				} else {
					unlink( "$dirname/$file" );
				}
			}
			$file = readdir( $dir_handle );
		}
		closedir( $dir_handle );
		rmdir( $dirname );
		return true;
	}

	/**
	 * Returns `true` if the database exists, else `false`.
	 *
	 * @since 1.0.0
	 * @return bool
	 */
	protected function does_database_exist() {

		$db      = $this->get_database_connection();
		$db_name = $this->get_opt->getOption( 'db-name' );

		$result = $db->query( "SHOW DATABASES WHERE `Database` = '$db_name';" );
		if ( $result === false ) {
			echo "DATABASE ERROR: $db->error\n";
		}
		$row = $result->fetch_row();
		return ! is_null( $row );
	}

	/**
	 * Download a file with cURL.
	 *
	 * @since 1.0.0
	 * @param string $url       What to download.
	 * @param string $file_name Where to save it.
	 */
	protected function download( $url, $file_name ) {

		echo "Downloading $url\n";

		// I have no idea why this works with the WordPress ZIP archive files,
		// but does not with https://develop.svn.wordpress.org/tags/6.0.2/wp-tests-config-sample.php.
		// phpcs:disable Generic.WhiteSpace.ScopeIndent.Incorrect
		// phpcs:disable Squiz.Commenting.InlineComment.SpacingBefore
//		$curl_handle = curl_init( $url );
//		$file_handle = fopen( $file_name, 'wb' );
//		curl_setopt( $curl_handle, CURLOPT_FILE, $file_handle );
//		curl_exec( $curl_handle );
		// phpcs:enable Generic.WhiteSpace.ScopeIndent.Incorrect
		// phpcs:enable Squiz.Commenting.InlineComment.SpacingBefore

		// phpcs:disable Generic.PHP.BacktickOperator.Found
		`curl -s "$url" > "$file_name"`;
	}

	/**
	 * Downloads the WordPress configuration file.
	 *
	 * @since 1.0.0
	 */
	protected function download_config() {

		$develop_url = $this->get_develop_url();
		$this->download(
			"$develop_url/wp-tests-config-sample.php",
			"$this->test_dir/wordpress-tests-lib/wp-tests-config.php"
		);
	}

	/**
	 * Downloads the WordPress PHPUnit test suite library.
	 *
	 * @since 1.0.0
	 */
	protected function download_suite() {

		$develop_url = $this->get_develop_url();
		$export_path = $this->format_path( $this->test_dir ) . '/wordpress-tests-lib';

		// phpcs:disabled Generic.PHP.BacktickOperator.Found
		$phpunit_url = "$develop_url/tests/phpunit";
		`svn export --quiet --ignore-externals $phpunit_url/includes/ $export_path/includes`;
		`svn export --quiet --ignore-externals $phpunit_url/data/ $export_path/data`;
	}

	/**
	 * Formats the path, including changes for CygWin.
	 *
	 * @since 1.0.0
	 * @param string $path The path to format.
	 * @return string
	 */
	protected function format_path( string $path ) {

		$path = str_replace( '\\', '/', $path );

		if (
			isset( $_SERVER['PWD'] ) &&
			strpos( $_SERVER['PWD'], '/cygdrive/' ) === 0 &&
			preg_match( '/^(\w):/', $path, $matches )
		) {
			$path = '/cygdrive/' . strtolower( $matches[1] ) . substr( $path, 2 );
		}

		return $path;
	}

	/**
	 * Returns a database connection.
	 *
	 * @since 1.0.0
	 * @return mysqli
	 */
	protected function get_database_connection() {

		if ( $this->db ) {
			return $this->db;
		}

		$db_user = $this->get_opt->getOption( 'db-user' );
		$db_pass = $this->get_opt->getOption( 'db-pass' );

		$host = explode( ':', $this->get_opt->getOption( 'db-host' ) );
		if ( empty( $host[1] ) ) {
			$host[1] = ini_get( 'mysqli.default_port' );
		}

		// phpcs:disabled WordPress.PHP.NoSilencedErrors.Discouraged
		$this->db = @new mysqli( $host[0], $db_user, $db_pass, '', $host[1] );
		if ( $this->db->connect_errno ) {
			echo $this->db->connect_error;
			exit( $this->db->connect_errno );
		}

		return $this->db;
	}

	/**
	 * Returns the default database host name.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_db_host_default() {

		return 'localhost';
	}

	/**
	 * Returns the name of the environment variable used to hold the database host.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_db_host_env_var() {

		return $this->get_env_var_prefix() . '_DB_HOST';
	}

	/**
	 * Returns the database host from an environment variable.
	 *
	 * @since 1.0.0
	 * @return string|false
	 */
	protected function get_db_host_from_env() {

		return getenv( $this->get_db_host_env_var() );
	}

	/**
	 * Returns the default database name.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_db_name_default() {

		return "test_$this->slug";
	}

	/**
	 * Returns the name of the environment variable used to hold the database name.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_db_name_env_var() {

		return $this->get_env_var_prefix() . '_DB_NAME';
	}

	/**
	 * Returns the database name from an environment variable.
	 *
	 * @since 1.0.0
	 * @return string|false
	 */
	protected function get_db_name_from_env() {

		return getenv( $this->get_db_name_env_var() );
	}

	/**
	 * Returns the name of the environment variable used to hold the database password.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_db_pass_env_var() {

		return $this->get_env_var_prefix() . '_DB_PASS';
	}

	/**
	 * Returns the database password from an environment variable.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_db_pass_from_env() {

		$db_pass     = getenv( $this->get_db_pass_env_var() );
		if ( $db_pass === false ) {
			$db_pass = '';
		}

		return $db_pass;
	}

	/**
	 * Returns the default database username.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_db_user_default() {

		return 'root';
	}

	/**
	 * Returns the name of the environment variable used to hold the database user.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_db_user_env_var() {

		return $this->get_env_var_prefix() . '_DB_USER';
	}

	/**
	 * Returns the database user from an environment variable.
	 *
	 * @since 1.0.0
	 * @return string|false
	 */
	protected function get_db_user_from_env() {

		return getenv( $this->get_db_user_env_var() );
	}

	/**
	 * Returns the WordPress development SVN repository URL based on the requested WordPress version.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_develop_url() {

		$url     = 'https://develop.svn.wordpress.org/';
		$version = $this->get_opt->getOption( 'wp-version' );

		if ( preg_match( '/^([0-9]+\.[0-9]+)-(beta|RC)[0-9]+$/', $version, $matches ) ) {
			// 6.0-RC1 -> branches/6.0
			$url .= "branches/$matches[1]";

		} elseif ( preg_match( '/^[0-9]+\.[0-9]+$/', $version ) ) {
			// 6.0 -> branches/6.0
			$url .= "branches/$version";

		} elseif ( preg_match( '/[0-9]+\.[0-9]+\.[0-9]+/', $version ) ) {

			if ( preg_match( '/([0-9]+\.[0-9]+)\.0/', $version, $matches ) ) {
				// 6.0.0 -> tags/6.0
				$url .= "tags/$matches[1]";
			} else {
				// 6.0.1 -> tags/6.0.1
				$url .= "tags/$version";
			}
		} elseif ( in_array( $version, [ 'nightly', 'trunk' ], true ) ) {
			$url .= 'trunk';

		} else {
			$version = $this->get_latest_version( 'WordPress' );
			$url    .= "tags/$version";
		}

		return $url;
	}

	/**
	 * Returns the prefix used with environment variable names.
	 *
	 * The prefix is made from the first letter of each part of the slug.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_env_var_prefix() {

		$db_pass_env = '';
		foreach ( explode( '-', $this->slug ) as $part ) {
			$db_pass_env .= $part[0];
		}

		return strtoupper( $db_pass_env );
	}

	/**
	 * Returns the latest tagged version of WordPress or a WordPress plugin.
	 *
	 * @since 1.0.0
	 * @param string $slug The plugin slug or 'WordPress'.
	 * @return string
	 */
	protected function get_latest_version( $slug ) {

		if ( isset( $this->latest_versions[ $slug ] ) ) {
			return $this->latest_versions[ $slug ];
		}

		if ( $slug === 'WordPress' ) {
			$file = "$this->test_dir/wordpress-latest.json";
			$this->download( 'https://api.wordpress.org/core/version-check/1.7/', $file );
		} else {
			$file = "$this->test_dir/$slug-latest.json";
			$this->download( "https://api.wordpress.org/plugins/info/1.0/$slug.json", $file );
		}

		$info = file_get_contents( $file );
		$info = json_decode( $info );

		if ( $slug === 'WordPress' ) {
			$version = reset( $info->offers )->version;
		} else {
			if ( property_exists( $info, 'error' ) ) {
				echo $info->error . PHP_EOL;
				exit( 404 );
			}
			$version = $info->version;
		}

		return $version;
	}

	/**
	 * Returns the default test directory.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_test_dir_default() {

		return dirname( $GLOBALS['_composer_autoload_path'], 4 ) . '/tmp';
	}

	/**
	 * Returns the name of the environment variable used to hold the WordPress version.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_test_dir_env_var() {

		return $this->get_env_var_prefix() . '_TEST_DIR';
	}

	/**
	 * Returns the WordPress version from an environment variable.
	 *
	 * @since 1.0.0
	 * @return string|false
	 */
	protected function get_test_dir_from_env() {

		return getenv( $this->get_test_dir_env_var() );
	}

	/**
	 * Returns the default WordPress version.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_wp_version_default() {

		return 'latest';
	}

	/**
	 * Returns the name of the environment variable used to hold the WordPress version.
	 *
	 * @since 1.0.0
	 * @return string
	 */
	protected function get_wp_version_env_var() {

		return $this->get_env_var_prefix() . '_WP_VERSION';
	}

	/**
	 * Returns the WordPress version from an environment variable.
	 *
	 * @since 1.0.0
	 * @return string|false
	 */
	protected function get_wp_version_from_env() {

		$wp_version_env = $this->get_wp_version_env_var();
		return getenv( $wp_version_env );
	}

	/**
	 * Installs a testing database.
	 *
	 * @since 1.0.0
	 */
	protected function install_database() {

		$db_name = $this->get_opt->getOption( 'db-name' );

		if ( $this->get_opt->getOption( 'db-skip' ) === 'yes' ) {
			echo "Skipping installation of the testing database ($db_name).\n";
			return;
		}

		$exists = $this->does_database_exist();
		if ( $exists ) {
			if ( $this->get_opt->getOption( 'db-delete' ) === 'yes' ) {
				$this->delete_database();
			} else {
				echo "Leaving the existing testing database ($db_name) in place.";
				return;
			}
		}

		echo "Installing the testing database ($db_name).\n";
		$this->create_database();
	}

	/**
	 * Installs a plugin into the WordPress testing application.
	 *
	 * @since 1.0.0
	 */
	protected function install_plugin() {

		$plugin_slug    = $this->get_opt->getOperand( 'plugin-slug' );
		$plugin_version = $this->get_opt->getOption( 'plugin-version' );

		if ( $plugin_version === 'latest' ) {
			$plugin_version = $this->get_latest_version( $plugin_slug );
		}

		$zip      = new ZipArchive();
		$zip_path = "$this->test_dir/$plugin_slug.$plugin_version.zip";

		if (
			file_exists( $zip_path ) &&
			$zip->open( $zip_path, ZipArchive::CHECKCONS ) === true
		) {
			echo basename( $zip_path ) . " is already downloaded.\n";
		} else {
			$this->download(
				"https://downloads.wordpress.org/plugin/$plugin_slug.$plugin_version.zip",
				$zip_path
			);
		}

		echo "Installing the $plugin_slug plugin ($plugin_version).\n";
		$zip->open( $zip_path );
		$zip->extractTo( "$this->test_dir/wordpress/wp-content/plugins" );
	}

	/**
	 * Installs the WordPress test suite library.
	 *
	 * @since 1.0.0
	 */
	protected function install_suite() {

		if ( file_exists( "$this->test_dir/wordpress-tests-lib" ) ) {
			echo "The WordPress test suite library is already installed.\n";
		} else {
			echo "Installing the WordPress test suite library.\n";
			$this->download_suite();
		}

		if ( file_exists( "$this->test_dir/wordpress-tests-lib/wp-tests-config.php" ) ) {
			echo "The WordPress test suite is already configured.\n";
		} else {
			echo "Configuring the WordPress test suite.\n";
			$this->download_config();
			$this->configure_wordpress();
		}
	}

	/**
	 * Installs the WordPress application into the test directory.
	 *
	 * @since 1.0.0
	 */
	protected function install_wordpress() {

		$version = $this->get_opt->getOption( 'wp-version' );
		if ( $version === 'latest' ) {
			$version = $this->get_latest_version( 'WordPress' );
		}

		if ( in_array( $version, [ 'nightly', 'trunk' ], true ) ) {

			echo "Installing the test WordPress application ($version).\n";
			$export_path = $this->format_path( $this->test_dir ) . '/wordpress';

			// phpcs:disable Generic.PHP.BacktickOperator.Found
			`svn export https://core.svn.wordpress.org/trunk $export_path`;

		} else {
			if ( file_exists( "$this->test_dir/wordpress/wp-includes/version.php" ) ) {

				/* @noinspection PhpIncludeInspection */
				include_once "$this->test_dir/wordpress/wp-includes/version.php";

				/* @var string $wp_version */
				if ( $version === $wp_version ) {
					echo "WordPress $version is already installed.\n";
					return;
				}
				$this->delete_directory( "$this->test_dir/wordpress" );
			}

			$zip      = new ZipArchive();
			$zip_path = "$this->test_dir/wordpress-$version.zip";

			if (
				file_exists( $zip_path ) &&
				$zip->open( $zip_path, ZipArchive::CHECKCONS ) === true
			) {
				echo basename( $zip_path ) . " is already downloaded.\n";
			} else {
				$this->download(
					"https://downloads.wordpress.org/release/wordpress-$version.zip",
					$zip_path
				);
			}

			echo "Installing the test WordPress application ($version).\n";
			$zip->open( $zip_path );
			$zip->extractTo( $this->test_dir );
		}
	}

	/**
	 * The main handler of this script.
	 *
	 * @since 1.0.0
	 */
	public function main() {

		// Process arguments and catch user errors.
		try {
			try {
				$this->get_opt->process();
			} catch ( Missing $exception ) {
				// Catch missing exceptions if help is requested.
				if ( ! $this->get_opt->getOption( 'help' ) ) {
					echo $exception->getMessage();
					exit( $exception->getCode() );
				}
			}
		} catch ( ArgumentException $exception ) {
			file_put_contents( 'php://stderr', $exception->getMessage() . PHP_EOL );
			echo PHP_EOL . $this->get_opt->getHelpText();
			exit;
		}

		// Show version and quit.
		if ( $this->get_opt->getOption( 'version' ) ) {
			echo sprintf( '%s: %s' . PHP_EOL, self::NAME, self::VERSION );
			exit;
		}

		// Show help and quit.
		$command = $this->get_opt->getCommand();
		if ( ! $command || $this->get_opt->getOption( 'help' ) ) {
			echo $this->get_opt->getHelpText();
			exit;
		}

		if ( ! file_exists( $this->test_dir ) ) {
			mkdir( $this->test_dir );
		}

		// Call the requested command.
		call_user_func( $command->getHandler(), $this->get_opt );
	}

	/**
	 * Sets up the command and options for this script.
	 *
	 * @since 1.0.0
	 */
	protected function set_up_get_opt() {

		$test_config = dirname( $GLOBALS['_composer_autoload_path'], 4 ) . '/test-config.php';
		if ( file_exists( $test_config ) ) {
			include $test_config;
		}

		$this->slug = basename( dirname( $GLOBALS['_composer_autoload_path'], 4 ) );

		$this->get_opt = new GetOpt( null, [ GetOpt::SETTING_STRICT_OPERANDS => true ] );

		$db_host         = $this->get_db_host_from_env();
		$db_host_default = $this->get_db_host_default();
		$db_host_env     = $this->get_db_host_env_var();
		$db_name         = $this->get_db_name_from_env();
		$db_name_default = $this->get_db_name_default();
		$db_name_env     = $this->get_db_name_env_var();
		$db_pass         = $this->get_db_pass_from_env();
		$db_pass_env     = $this->get_db_pass_env_var();
		$db_user         = $this->get_db_user_from_env();
		$db_user_default = $this->get_db_user_default();
		$db_user_env     = $this->get_db_user_env_var();

		$db_options = [
			Option::create( null, 'db-host', GetOpt::REQUIRED_ARGUMENT )
				->setDefaultValue( $db_host ? $db_host : $db_host_default )
				->setDescription( "The database host ($db_host_env environment variable or $db_host_default)." ),
			Option::create( null, 'db-user', GetOpt::REQUIRED_ARGUMENT )
				->setDefaultValue( $db_user ? $db_user : $db_user_default )
				->setDescription( "The database user ($db_user_env environment variable or $db_user_default)." ),
			Option::create( null, 'db-pass', GetOpt::REQUIRED_ARGUMENT )
				->setDefaultValue( $db_pass )
				->setDescription( "The database password ($db_pass_env environment variable)." ),
			Option::create( null, 'db-name', GetOpt::REQUIRED_ARGUMENT )
				->setDefaultValue( $db_name ? $db_name : $db_name_default )
				->setDescription( "The database name ($db_name_env environment variable or $db_name_default)." ),
			Option::create( null, 'db-delete', GetOpt::REQUIRED_ARGUMENT )
				->setDefaultValue( 'yes' )
				->setDescription( 'Delete existing database before recreating it, yes|no (yes).' )
				->setValidation( [ $this, 'validate_yes_no' ] ),
			Option::create( null, 'db-skip', GetOpt::REQUIRED_ARGUMENT )
				->setDefaultValue( 'no' )
				->setDescription( 'Skip creating the database, yes|no (no).' )
				->setValidation( [ $this, 'validate_yes_no' ] ),
		];

		$wp_version         = $this->get_wp_version_from_env();
		$wp_version_default = $this->get_wp_version_default();
		$wp_version_env     = $this->get_wp_version_env_var();

		$wp_version = Option::create( null, 'wp-version', GetOpt::REQUIRED_ARGUMENT )
			->setDefaultValue( $wp_version ? $wp_version : $wp_version_default )
			->setDescription(
				'The version of WordPress to install ' .
				"($wp_version_env environment variable or $wp_version_default)."
			);

		$test_dir         = $this->get_test_dir_from_env();
		$test_dir_default = $this->get_test_dir_default();
		$test_dir_env     = $this->get_test_dir_env_var();
		$this->test_dir   = $test_dir ? $test_dir : $test_dir_default;

		// Global options.
		/* @noinspection PhpRedundantOptionalArgumentInspection */
		$this->get_opt->addOptions(
			[
				Option::create( 'd', 'directory', GetOpt::REQUIRED_ARGUMENT )
					->setDefaultValue( $this->test_dir )
					->setDescription( "The test directory ($test_dir_env environment variable or $test_dir_default)." ),

				Option::create( '?', 'help', GetOpt::NO_ARGUMENT )
					->setDescription( 'Show this help and quit.' ),

				Option::create( null, 'version', GetOpt::NO_ARGUMENT )
					->setDescription( 'Show version information and quit.' ),
			]
		);

		/* @noinspection GrazieInspection */
		$this->get_opt->addCommands(
			[
				Command::create( 'install-db', [ $this, 'install_database' ] )
					->setShortDescription( 'Installs a test database.' )
					->addOptions( $db_options ),

				Command::create( 'install-plugin', [ $this, 'install_plugin' ] )
					->setDescription(
						'Installs a WordPress plugin into the testing environment.'
					)
					->addOperand( Operand::create( 'plugin-slug', Operand::REQUIRED ) )
					->addOption(
						Option::create( null, 'plugin-version', GetOpt::REQUIRED_ARGUMENT )
							->setDefaultValue( 'latest' )
							->setDescription( 'The version of the plugin to install (latest).' )
					),

				Command::create( 'install-suite', [ $this, 'install_suite' ] )
					->setDescription( 'Installs the WordPress test suite library.' )
					->addOptions( $db_options ),

				Command::create( 'install-wp', [ $this, 'install_wordpress' ] )
					->setDescription(
						'Install the WordPress application into the test directory.'
					)
					->addOption( $wp_version ),

				Command::create(
					'install-wp-suite-db',
					function () {
						$this->install_wordpress();
						$this->install_suite();
						$this->install_database();
					}
				)
					->setDescription(
						'Installs WordPress, the test suite, and a test database.'
					)
					->addOption( $wp_version )
					->addOptions( $db_options ),

				Command::create( 'uninstall', [ $this, 'uninstall' ] )
					->setDescription( 'Uninstalls the WordPress testing environment and database.' ),
			]
		);
	}

	/**
	 * Uninstalls the testing environment.
	 *
	 * @since 1.0.0
	 */
	protected function uninstall() {

		$this->delete_directory( $this->test_dir );
		$this->delete_database();
	}

	/**
	 * Returns `true` if the value is 'yes' or 'no', else `false`.
	 *
	 * @since 1.0.0
	 * @param string $value The value to validate.
	 * @return bool
	 */
	public function validate_yes_no( $value ) {

		return in_array( $value, [ 'yes', 'no' ], true );
	}
}
